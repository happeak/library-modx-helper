<?php

use ModxLibrary\ModxShop;

require_once "../vendor/autoload.php";

require_once "../../../brands/cosatto/config.core.php";
require_once MODX_CORE_PATH . "model/modx/modx.class.php";

$modx = new modX();
$modx->initialize('web');

$shop = new ModxShop($modx);
$shop->getProductManager()->syncProducts();
