<?php

namespace ModxLibrary\Manager;

use ModxLibrary\ModxShop;

class TemplateManager extends ModxShop implements TemplateManagerInterface
{

    /**
     * Получить ID шаблона по его имени
     *
     * @param $templateName
     * @return mixed
     */
    public function getTemplateByName($templateName)
    {
        return $this->shop->getObject('modTemplate', ['templatename' => $templateName]);
    }
}