<?php

namespace ModxLibrary\Manager;

interface TemplateManagerInterface
{
    /**
     * Получить ID шаблона по его имени
     *
     * @param $templateName
     * @return mixed
     */
    public function getTemplateByName($templateName);
}