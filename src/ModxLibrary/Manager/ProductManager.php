<?php

namespace ModxLibrary\Manager;

use Happeak\HappeakApiLibrary;
use ModxLibrary\ModxShop;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class ProductManager extends ModxShop implements ProductManagerInterface
{
    /**
     * Добавление нового товара в магазин
     *
     * @param $product
     * @return mixed|void
     */
    public function addProduct($product)
    {
        $logger = new Logger('add_product');
        $logger->pushHandler(new StreamHandler("{$this->logsPath}/create.log", Logger::INFO));

        $templateManager = new TemplateManager($this->shop);
        $template = $templateManager->getTemplateByName('Товар');

        $object = $this->shop->newObject('msProduct', [
            'parent' => 0,
            'template' => $template->get('id'),
            'source' => 2,
            'pagetitle' => $product['name'],
            'article' => $product['id'],
            'price' => $product['price'],
            'old_price' => $product['old_price'],
            'published' => 0,
            'show_in_tree' => 1,
            'content' => $product['description'],
        ]);

        if ($object->save()) {
            $object->setTVValue('stock', $product['stock']);

            $this->loadProductImages($product);

            $object->save();
        } else {
            $logger->addAlert('Не удалось добавить товар');
        }
    }

    /**
     * Обновление информации о товаре
     *
     * @param $product
     * @return mixed|void
     */
    public function updateProduct($product)
    {
        $logger = new Logger('sync_logger');
        $logger->pushHandler(new StreamHandler("{$this->logsPath}/update.log", Logger::INFO));

        $data = $this->shop->getObject('msProductData', ['article' => $product['id']]);
        if ($data) {
            $object = $data->getOne('Product');
            $title = $object->get('pagetitle');

            if ((int) $object->get('price') !== (int) $product['price']) {
                $object->set('price', $product['price']);

                $logger->addInfo("Обновлена текущая цена товара {$title}");
            }
            if ((int) $object->get('old_price') !== (int) $product['base_price']) {
                $object->set('old_price', $product['base_price']);

                $logger->addInfo("Обновлена старая цена товара {$title}");
            }
            if ((int) $object->getTVValue('stock') !== (int) $product['stock']) {
                $object->setTVValue('stock', $product['stock']);

                $logger->addInfo("Обновлено наличие товара {$title}");
            }

            $object->save();
        }
    }

    /**
     * Получение списка товаров по заданным критериям
     *
     * @param array $criteria
     * @return mixed
     */
    public function getProducts($criteria = [])
    {
        $products = $this->shop->getCollection('msProduct', $criteria);
        return $products;
    }

    /**
     * Получение списка артикулов товаров (= коды товаров на Happeak.ru)
     *
     * @param array $criteria
     * @return array
     */
    public function getProductArticles($criteria = [])
    {
        $ids = [];
        foreach ($this->getProducts($criteria) as $product) {
            $ids[] = $product->get('article');
        }

        return $ids;
    }

    /**
     * @param $product
     */
    public function loadProductImages($product)
    {
        $data = $this->shop->getObject('msProductData', ['article' => $product['id']]);
        $object = $data->getOne('Product');

        $logger = new Logger('load_images_logger');
        $logger->pushHandler(new StreamHandler("{$this->logsPath}/load_images.log", Logger::INFO));

        $temporaryFolder = $this->shop->getOption('base_path'). 'tmp/';

        foreach ($product['images'] as $image) {
            // папка для сохранения картинок текущего товара
            $imagesPath = $temporaryFolder . $product['id'];
            if (!file_exists($imagesPath)) {
                mkdir($imagesPath, 0777);
            }
            copy($image, $imagesPath . '/' . basename($image));

            $file = $imagesPath . '/' . basename($image);

            $response = $this->shop->runProcessor('gallery/upload',
                ['id' => $object->get('id'), 'file' => $file],
                ['processors_path' => $this->shop->getOption('core_path').'components/minishop2/processors/mgr/']
            );

            if ($response->isError()) {
                $logger->addAlert("Error on upload \"$file\": \n". print_r($response->getAllErrors(), 1));
            }
            else {
                $logger->addInfo("Successful upload  \"$file\": \n". print_r($response->getObject(), 1));
            }
        }
    }

    /**
     * Синхронизация цен и наличия
     * @return string
     */
    public function syncProducts()
    {
        $logger = new Logger('sync_logger');
        $logger->pushHandler(new StreamHandler("{$this->logsPath}/update.log", Logger::INFO));

        $happeak = new HappeakApiLibrary($this->getParameters());
        $productChunks = array_chunk($this->getProductManager()->getProductArticles(['published' => 1]), 100);

        foreach ($productChunks as $productChunk) {
            $actualProducts = $happeak->getProductService($happeak)->getMany($productChunk);

            foreach ($actualProducts as $actualProduct) {
                $this->updateProduct($actualProduct);
            }
        }

        $logger->addInfo('Синхронизация завершена');

        $this->clearCache();

        return;
    }
}