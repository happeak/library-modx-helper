<?php

namespace ModxLibrary\Manager;

interface ProductManagerInterface
{
    /**
     * Добавление нового товара в магазин
     *
     * @param $product
     * @return mixed
     */
    public function addProduct($product);

    /**
     * Обновление информации о товаре
     *
     * @param $product
     * @return mixed
     */
    public function updateProduct($product);

    /**
     * Получение товаров магазина
     *
     * @param array $criteria
     * @return mixed
     */
    public function getProducts($criteria = []);

    /**
     * Получение массива ID товаров
     *
     * @param array $criteria
     * @return mixed
     */
    public function getProductArticles($criteria = []);

    /**
     * Синхронизация цен и наличия товаров
     * @return mixed
     */
    public function syncProducts();
}