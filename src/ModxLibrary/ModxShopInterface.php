<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 12.01.17
 * Time: 11:50
 */

namespace ModxLibrary;

interface ModxShopInterface
{
    /**
     * Получение параметров магазина (ID и ключ)
     *
     * @return mixed
     */
    public function getParameters();

    /**
     * Очистка кэша
     *
     * @return mixed
     */
    public function clearCache();
}