<?php

namespace ModxLibrary;

use ModxLibrary\Manager\ProductManager;
use ModxLibrary\Manager\TemplateManager;

class ModxShop implements ModxShopInterface
{
    protected $logsPath;
    protected $shop;

    public function __construct($modx)
    {
        $this->shop = $modx;
        $this->logsPath = $modx->getOption('core_path') . "/../logs";
    }

    /**
     * Очистка кэша
     *
     * @return mixed
     */
    public function clearCache()
    {
        return $this->shop->cacheManager->refresh();
    }

    /**
     * Получение параметров магазина (ID и ключ)
     *
     * @return array
     */
    public function getParameters()
    {
        return [
            'id' => $this->shop->getOption('api_shop_id'),
            'key' => $this->shop->getOption('api_shop_key'),
        ];
    }

    /**
     * Возвращает объект менеджера товаров
     *
     * @return ProductManager
     */
    public function getProductManager()
    {
        return new ProductManager($this->shop);
    }

    /**
     * Возвращает объект менеджера шаблонов
     *
     * @return TemplateManager
     */
    public function getTemplateManager()
    {
        return new TemplateManager($this->shop);
    }

    /**
     * Очистка временной папки для подгрузки картинок
     */
    public function clearTemporaryFolder()
    {
        $files = glob($this->shop->getOption('base_path') . 'tmp/*');

        foreach ($files as $file) {
            if (is_dir($file)) {
                $innerFiles = glob($file . "/*");
                foreach ($innerFiles as $innerFile) {
                    unlink($innerFile);
                }
                rmdir($file);
            } else {
                unlink($file);
            }
        }
    }
}